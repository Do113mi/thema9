package packet;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import domain.City;
import service.CityRepository;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class App implements CommandLineRunner{
	@Autowired
	private CityRepository cityRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
	@Override
	public void run(String... arg0) throws Exception {
		for (City c : cityRepository.findAll()){
			System.out.println(c);
		};
	}
	
//	public void add() 
//	{
//		EntityManager em = Persistence.createEntityManagerFactory("jpaTestPU").createEntityManager();
//		em.getTransaction().begin();
//		
//		em.persist(nc);
//        em.getTransaction().commit();
//        
//        em.close();
//	}
	

}