
package service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import domain.City;



public interface CityRepository extends CrudRepository<City, Integer>{
	List<City> findAll(); 
}

//public interface CityRepository extends Repository<City, Long> {
//
//	Page<City> findAll(Pageable pageable);
//
//	Page<City> findByNameContainingAndCountryContainingAllIgnoringCase(String name,
//			String country, Pageable pageable);
//
//	City findByNameAndCountryAllIgnoringCase(String name, String country);
//
//}